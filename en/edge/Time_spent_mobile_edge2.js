
(function(compId){"use strict";var _=null,y=true,n=false,x13='break-word',x27='violator',x12='freight-sans-pro, sans-serif',x28='91px',cl='clip',x32='51px',x5='rgba(105,167,82,1)',e16='${RectangleCopy9}',x='text',x11='9',m='rect',e18='${Group4}',x23='549px',i='none',x3='rgba(192,192,192,1)',x31='28px',p='px',o='opacity',x29='78px',x6='rgba(0,0,0,0)',x25='auto',x2='5.0.0.375',l='normal',x24='247px',e20='${RectangleCopy14}',x14='11px',x22='0px',x21='copy',x15='rgba(255,255,255,1)',x4='rgba(105,167,82,1.00)',x33='mobile',e19='${Text}',x1='5.0.0',xc='rgba(0,0,0,1)',g='image',x17='rect(@@0@@px @@1@@px @@2@@px @@3@@px)';var g8='Pasted.svg',g26='copy.svg',g30='violator.svg',g34='mobile.svg',g7='violator3.svg',g9='mobile_data2.svg';var s10="Source: Monthly data calculated from comScore Key Measures and Mobile Metrix US, October 2014";var im='course/en/edge/Time_spent_mobile_images/',aud='media/',vid='media/',js='js/',fonts={'freight-sans-pro, sans-serif':'<script src=\"//use.typekit.net/keg2ebw.js\"></script><script>try{Typekit.load();}catch(e){}</script>'},opts={'gAudioPreloadPreference':'auto','gVideoPreloadPreference':'auto'},resources=[],scripts=[],symbols={"stage":{v:x1,mv:x1,b:x2,stf:i,cg:i,rI:n,cn:{dom:[{id:'rectangles',t:'group',r:['51px','48px','483','185','auto','auto'],c:[{id:'RectangleCopy',t:m,r:['62px','124px','42px','61px','auto','auto'],f:[x3],s:[0,xc,i]},{id:'RectangleCopy2',t:m,r:['125px','134px','42px','51px','auto','auto'],f:[x3],s:[0,xc,i]},{id:'RectangleCopy3',t:m,r:['188px','143px','42px','42px','auto','auto'],f:[x3],s:[0,xc,i]},{id:'RectangleCopy4',t:m,r:['250px','145px','42px','40px','auto','auto'],f:[x3],s:[0,xc,i]},{id:'RectangleCopy5',t:m,r:['314px','145px','42px','40px','auto','auto'],f:[x3],s:[0,xc,i]},{id:'RectangleCopy6',t:m,r:['378px','161px','42px','24px','auto','auto'],f:[x3],s:[0,xc,i]},{id:'RectangleCopy8',t:m,r:['439px','162px','42px','23px','auto','auto'],f:[x3],s:[0,xc,i]},{id:'Rectangle',t:m,r:['0px','0px','42px','185px','auto','auto'],f:[x3],s:[0,xc,i]}]},{id:'Group12',t:'group',r:['51px','48px','42','185','auto','auto'],c:[{id:'RectangleCopy14',t:m,r:['0px','0px','42px','185px','auto','auto'],cl:'rect(0px 42px 185px 0px)',f:[x4],s:[0,xc,i]}]},{id:'Group4',t:'group',r:['370','33','126','126','auto','auto'],o:0,c:[{id:'Ellipse',t:'ellipse',r:['0px','0px','126px','126px','auto','auto'],br:["50%","50%","50%","50%"],f:[x5],s:[0,"rgb(0, 0, 0)",i]},{id:'violator3',t:g,r:['12px','20px','102px','89px','auto','auto'],f:[x6,im+g7,'0px','0px']}]},{id:'Pasted',t:g,r:['265px','36px','53px','49px','auto','auto'],f:[x6,im+g8,'0px','0px']},{id:'RectangleCopy9',t:m,r:['113px','172px','42px','61px','auto','auto'],cl:'rect(0px 42px 61px 0px)',f:[x4],s:[0,xc,i]},{id:'mobile_data2',t:g,r:['10px','10px','544px','242px','auto','auto'],f:[x6,im+g9,'0px','0px']},{id:'mobile2',symbolName:'mobile2',t:m,r:['278px','39px','28','51','auto','auto'],tf:[[],[],[],['1.11516','1.11516']]},{id:'Text',t:x,r:['29px','258px','399px','14px','auto','auto'],text:s10,n:[x12,[x11,p],"rgba(82,82,82,1.00)",l,i,"",x13,l],ts:["","",x14,""]}],style:{'${Stage}':{isStage:true,r:['null','null','600px','280px','auto','auto'],overflow:'hidden',f:[x15]}}},tt:{d:3000,a:n,data:[["eid484",cl,1500,250,"linear",e16,[61.105712890625,42,61,0],[0,42,61,0],{vt:x17}],["eid487",o,2000,750,"linear",e18,'0','1'],["eid488","line-height",3000,0,"linear",e19,'11px','11px'],["eid435",cl,1500,250,"linear",e20,[185.490234375,42,185,0],[0,42,185,0],{vt:x17}]]}},"Symbol_1":{v:x1,mv:x1,b:x2,stf:i,cg:i,rI:n,cn:{dom:[{id:x21,r:[x22,x22,x23,x24,x25,x25],t:g,f:[x6,im+g26,x22,x22]}],style:{'${symbolSelector}':{r:[_,_,x23,x24]}}},tt:{d:0,a:y,data:[]}},"violator":{v:x1,mv:x1,b:x2,stf:i,cg:i,rI:n,cn:{dom:[{id:x27,r:[x22,x22,x28,x29,x25,x25],t:g,f:[x6,im+g30,x22,x22]}],style:{'${symbolSelector}':{r:[_,_,x28,x29]}}},tt:{d:0,a:y,data:[]}},"mobile2":{v:x1,mv:x1,b:x2,stf:i,cg:i,rI:n,cn:{dom:[{r:[x22,x22,x31,x32,x25,x25],id:x33,t:g,f:[x6,im+g34,x22,x22]}],style:{'${symbolSelector}':{r:[_,_,x31,x32]}}},tt:{d:0,a:y,data:[]}}};AdobeEdge.registerCompositionDefn(compId,symbols,fonts,scripts,resources,opts);})("EDGE-c-2040");
(function($,Edge,compId){var Composition=Edge.Composition,Symbol=Edge.Symbol;Edge.registerEventBinding(compId,function($){
//Edge symbol: 'stage'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",3000,function(sym,e){});
//Edge binding end
})("stage");
//Edge symbol end:'stage'

//=========================================================

//Edge symbol: 'Symbol_1'
(function(symbolName){})("Symbol_1");
//Edge symbol end:'Symbol_1'

//=========================================================

//Edge symbol: 'violator'
(function(symbolName){})("violator");
//Edge symbol end:'violator'

//=========================================================

//Edge symbol: 'mobile2'
(function(symbolName){})("mobile2");
//Edge symbol end:'mobile2'
})})(AdobeEdge.$,AdobeEdge,"EDGE-c-2040");